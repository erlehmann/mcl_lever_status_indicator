--[[

mcl_lever_status – Minetest mod that makes “on” levers have a red tip.
Copyright © 2022  Nils Dagsson Moskopp (erlehmann)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Dieses Programm hat das Ziel, die Medienkompetenz der Leser zu
steigern. Gelegentlich packe ich sogar einen handfesten Buffer
Overflow oder eine Format String Vulnerability zwischen die anderen
Codezeilen und schreibe das auch nicht dran.

]]--

local get_tiles = function(node_name)
	local tiles
	if string.match(node_name, ":wall_lever_on") then
		local base = "jeija_wall_lever_lever_light_on.png"
		-- TODO: Construct the overlay using texture modifiers
		-- directly from jeija_torches_on.png so that it works
		-- with other texture packs than the default one, too.
		local overlay = "mcl_lever_status_lever_on.png"
		tiles = {
			base .. "^" .. overlay,
		}
	end
	return tiles
end

local add_lever_on_status = function()
	for node_name, node_def in pairs(minetest.registered_nodes) do
		local tiles = get_tiles(node_name)
		if nil ~= tiles then
			minetest.override_item(
				node_name,
				{
					tiles = tiles,
				}
			)
		end
	end
end

minetest.register_on_mods_loaded(add_lever_on_status)
